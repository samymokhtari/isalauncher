/* SCRIPT VERSION 1.0 : FILL ALL THE TABLES */

/* CREATION OF TABLES */
CREATE TABLE IF NOT EXISTS Category (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT ,
                name VARCHAR(50) NOT NULL);

CREATE TABLE IF NOT EXISTS Software (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                name VARCHAR(100) NOT NULL,
                idCategory BIGINT NOT NULL,
                FOREIGN KEY(idCategory) REFERENCES Category(id));
				
CREATE TABLE IF NOT EXISTS Version (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                name VARCHAR(50) NOT NULL,
                path VARCHAR(500) NOT NULL,
				arguments VARCHAR(500),
                service VARCHAR(100) NOT NULL,
                idSoftware BIGINT NOT NULL,
                FOREIGN KEY(idSoftware) REFERENCES Software(id));


/* INSERTION */

/* Category */
INSERT INTO Category(name) VALUES('Prestataire'); 
INSERT INTO Category(name) VALUES('Individuel');
INSERT INTO Category(name) VALUES('Gestion électronique des documents');

/* Software */
INSERT INTO Software(name, idCategory) VALUES( 'PREST 13.60.400', 1);
INSERT INTO Software(name, idCategory) VALUES( 'PREST 13.83.300', 1);

INSERT INTO Software(name, idCategory) VALUES( 'ISAGED 2.10.400', 3);
INSERT INTO Software( name, idCategory) VALUES( 'ISAGED 2.20.100', 3);
INSERT INTO Software( name, idCategory) VALUES( 'ISAGED 2.50.000', 3);

INSERT INTO Software( name, idCategory) VALUES( 'IsaCompta 12.63.001', 2);
INSERT INTO Software( name, idCategory) VALUES( 'IsaCompta 13.60.400', 2);
INSERT INTO Software( name, idCategory) VALUES( 'IsaCompta 13.70.101', 2);

/* Version */
INSERT INTO Version( name, path, service, idSoftware) VALUES( 'ISACOMPTA 13.60.400', 'C:\prest\1360400\isacowp.gi\Client\CO.Application.exe', 'IS-CP-13.60.400', 1);
INSERT INTO Version( name, path, service, idSoftware) VALUES( 'ISAGI 13.60.400', 'C:\prest\1360400\isagiwp\Client\GI.Application.exe', 'IS-GI-13.60.400', 1);

INSERT INTO Version( name, path, service, idSoftware) VALUES( 'ISACOMPTA 13.83.400', 'C:\prest\1383300\isagiwp\Client\CO.Application.exe', 'IS-CP-13.83.400', 2);
INSERT INTO Version( name, path, service, idSoftware) VALUES( 'ISAGI 13.83.400', 'C:\prest\1383300\isagiwp\Client\GI.Application.exe', 'IS-GI-13.83.400', 2);

INSERT INTO Version( name, path, arguments, service, idSoftware) VALUES( 'ISAGED 2.10.400', 'D:\210400\ISAGDWP\Client\GD.Application.exe', '/IP.Authentication.DomainId=1 /IP.Authentication.DataBaseName=TEST1 /IP.Authentication.Login=gleclerc /IP.Authentication.Password=adminged /IP.Authentication.RadicalApplication=GD', 'IS-GD-2.10.400', 3);
/* AUTH : */
INSERT INTO Version( name, path, arguments, service, idSoftware) VALUES( 'ISAGED 2.20.100', 'D:\IsaGDWP\Client\GD.Application.exe', '/IP.Authentication.DataSetLabel=TEST1 /IP.Authentication.DomainId=6289 /IP.Authentication.Login=adminged /IP.Authentication.Password=', 'IS-GD-2.20.100', 4);

INSERT INTO Version( name, path, arguments, service, idSoftware) VALUES( 'ISAGED 2.50.000', 'D:\2_50\ISAGDWP\Client\GD.Application.exe' , '/IP.Authentication.DataSetLabel=GED_2.50.000 /IP.Authentication.DomainId=6289 /IP.Authentication.Login=gleclerc /IP.Authentication.Password=', 'IS-GD-2.50.000', 5);

INSERT INTO Version( name, path, service, idSoftware) VALUES( 'ISACOMPTA 12.63.001', 'C:\Compta\V2015V6\isacowp\Client\CO.Application.exe', 'IS-Isacompta-12.63.001', 6);

INSERT INTO Version( name, path, service, idSoftware) VALUES( 'ISACOMPTA 13.60.400', 'C:\Compta\V2018\isacowp\Client\CO.Application.exe', 'IS-CO-13.60.400', 7);

INSERT INTO Version( name, path, service, idSoftware) VALUES( 'GC-ISACOMPTA 13.70.101', 'C:\FACT\V2018\isacopgc\Client\CO.Application.exe', 'IS-CO-GC-13.70.011', 8);
INSERT INTO Version( name, path, service, idSoftware) VALUES( 'Gestion Commercial 13.70.101', 'C:\FACT\V2018\IsaGCWp\Client\GC.Application.exe', 'IS-GC-13.70.101', 8);