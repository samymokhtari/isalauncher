﻿using System;
using System.Collections.Generic;

namespace IsaSofts.Core
{
    public class IsaVersion
    {
        // Méthodes
        private string _name;
        private int _id;
        private string _path;
        private string _arguments;
        private string _service;

        // Constructors
        /// <summary>
        /// Do work function
        /// </summary>
        /// <param name="id">This is the id of the version in the database</param>
        /// <param name="name">This is the version number.</param>
        /// <param name="path">This is the path of the version (example: C:/softwares/software1.exe).</param>
        /// <param name="service">This is the windows service required for the software.</param>
        public IsaVersion(int p_id, string p_name, string p_path, string p_arguments, string p_service)
        {
            Id = p_id;
            Name = p_name;
            Path = p_path;
            Arguments = p_arguments;
            Service = p_service;
        }

        public string Name { get => _name; set => _name = value; }
        public int Id { get => _id; set => _id = value; }
        public string Path { get => _path; set => _path = value; }
        public string Service { get => _service; set => _service = value; }
        public string Arguments { get => _arguments; set => _arguments = value; }

        //Methods
        public override string ToString()
        {
            return Name;
        }

        
    }
}
