﻿using System.Collections.Generic;


namespace IsaSofts.Core
{
    public class IsaSoftware
    {
        // Attributes
        private int _id;
        private string _name;
        private List<IsaVersion> _versions;

        public string Name { get => _name; set => _name = value; }
        public List<IsaVersion> Versions { get => _versions; set => _versions = value; }
        public int Id { get => _id; set => _id = value; }

        // Constructors
        /// <summary>
        /// Constructor of IsaSoftware
        /// </summary>
        /// <param name="name">This is the name of the software.</param>
        /// <param name="command">This is a list of the version available for this software.</param>
        public IsaSoftware(int p_id, string p_name)
        {
            Id = p_id;
            Name = p_name;
        }
           public IsaSoftware(int p_id, string p_name, List<IsaVersion> p_versions)
        {
            Id = p_id;
            Name = p_name;
            Versions = p_versions;
        }

        // Methods
        public override string ToString()
        {
            return Name;
        }


        
    }
}
