﻿using System;
using System.IO;

namespace IsaSofts.Core.Tools
{
    public class Log
    {
        StreamWriter _writer;
        private readonly string PATH_LOG = $@"./logs";

        public Log()
        {
            Directory.CreateDirectory(PATH_LOG);
        }

        public void Write(string p_level, string p_message)
        {
            _writer = File.AppendText($"{PATH_LOG}/logs-{DateTime.Now.ToString("ddMMyy")}.log");
            _writer.WriteLine($"{p_level} : {DateTime.Now.ToShortTimeString()} - {p_message}");
            _writer.Flush();
            _writer.Close();
        }
    }
}
