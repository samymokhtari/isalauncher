﻿using System;

namespace IsaSofts.Core.Tools
{
    public sealed class DatabaseManagerSingleton
    {
        private DatabaseManagerSingleton()
        { }

        private static DatabaseManager _instance;

        public static DatabaseManager GetInstance()
        {
            if (_instance == null)
            {
                ConnectInstanceToDatabase();
                _instance = new DatabaseManager();
            }
            return _instance;
        }

        // Finally, any singleton should define some business logic, which can
        // be executed on its instance.
        private static void ConnectInstanceToDatabase()
        {
            try
            {
                _instance = new DatabaseManager();
                _instance.ConnectDB("base");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}