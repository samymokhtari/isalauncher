﻿using Ardalis.GuardClauses;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;


namespace IsaSofts.Core.Tools
{
    public sealed class DatabaseManager
    {
        // Attributes
        private SQLiteConnection _connection;
        private SQLiteCommand _command;
        private SQLiteDataReader _reader = null;
        private Log _log;
        private readonly string PATH_DATABASE = @"./database";

        // Declaration of constructors
        public DatabaseManager()
        {
            _log = new Log();
            _connection = new SQLiteConnection();
            _command = new SQLiteCommand();
            _command.Connection = _connection;
        }

        public void ConnectDB(string p_databaseName)
        {
            if(p_databaseName.Contains(".sqlite3") || p_databaseName.Contains(".sql")){
                p_databaseName = p_databaseName.Substring(0, p_databaseName.IndexOf(".sql"));
            }
            if (!(File.Exists($"{PATH_DATABASE}/{p_databaseName}.sqlite3"))){
                Directory.CreateDirectory(PATH_DATABASE);
                _connection.ConnectionString = $"Driver=SQLite3 ODBC Driver;Datasource={PATH_DATABASE}/{p_databaseName}.sqlite3;";
                _connection.Open();
                _log.Write("INFO", $"Database {p_databaseName}.sqlite3 created.");

                string script = Resource.script_data.ToString();
                ExecuteReader(script);
                _reader.Close();
                _log.Write("INFO", $"Script executed, all the tables created and the data inserted.");
            }
            else
            {
                _connection.ConnectionString = $"Driver=SQLite3 ODBC Driver;Datasource={PATH_DATABASE}/{p_databaseName}.sqlite3;";
                _connection.Open();
            }
        }

        public void CloseDB()
        {
            _command.Dispose();
            _connection.Close();
        }

        private void ExecuteReader(string p_Request)
        {
            try{
                _command.CommandText = p_Request;
                _reader = _command.ExecuteReader();
            }
            catch (SQLiteException e){
                throw e;
            }
        }

        private int Execute(string p_Request, List<string> p_Args = null)
        {
            try
            {
                _command.CommandText = p_Request;
                return _command.ExecuteNonQuery();
            }
            catch (SQLiteException e)
            {
                throw e;
            }
        }

        public List<Category> FetchCategories()
        {
            List<Category> v_categories = new List<Category>();
            List<string[]> v_readerCategory = new List<string[]>();
            ExecuteReader(@"SELECT DISTINCT Category.id, Category.name from Category;");
            while (_reader.Read())
            {
                v_readerCategory.Add(new string[]{ _reader.GetInt32(0).ToString(), _reader.GetString(1) });
            }
            _reader.Close();
            foreach(string[] row in v_readerCategory)
            {
                v_categories.Add(new Category(int.Parse(row[0]), row[1], FetchSoftwares(int.Parse(row[0])) ));
            }
            return v_categories;
        }

        public List<IsaSoftware> FetchSoftwares(int p_category)
        {
            List<IsaSoftware> v_softwares = new List<IsaSoftware>();
            ExecuteReader($@"SELECT DISTINCT Software.id, Software.name FROM Software INNER JOIN Category ON Software.idCategory = {p_category};");
            List<string[]> v_readerSoftware = new List<string[]>();
            while (_reader.Read())
            {
                v_readerSoftware.Add(new string[] { _reader.GetInt32(0).ToString(), _reader.GetString(1)});
            }
            _reader.Close();
            foreach (string[] row in v_readerSoftware)
            {
                v_softwares.Add(new IsaSoftware(int.Parse(row[0]), row[1], FetchVersions(int.Parse(row[0])) ) );
            }
            return v_softwares;
        }

        public List<IsaVersion> FetchVersions(int p_software)
        {
            List<IsaVersion> v_versions = new List<IsaVersion>();
            List<string[]> v_readerVersion = new List<string[]>();
            ExecuteReader($@"SELECT DISTINCT Version.id, Version.name, Version.path, Version.arguments, Version.service FROM Version INNER JOIN Software ON Version.idSoftware = {p_software};");
            while (_reader.Read())
            {
                if (!_reader.IsDBNull(3))
                {
                    v_readerVersion.Add(new string[] { _reader.GetInt32(0).ToString(), _reader.GetString(1), _reader.GetString(2), _reader.GetString(3), _reader.GetString(4) });
                }
                else
                {
                    v_readerVersion.Add(new string[] { _reader.GetInt32(0).ToString(), _reader.GetString(1), _reader.GetString(2), " ", _reader.GetString(4) });
                }
                    
            }
            _reader.Close();
            foreach (string[] row in v_readerVersion)
            {
                v_versions.Add( new IsaVersion(int.Parse(row[0]), row[1], row[2], row[3], row[4]) );
            }

            return v_versions;
        }

        public List<string> FetchService(int p_software)
        {
            List<string> v_services = new List<string>();
            List<string[]> v_readerVersion = new List<string[]>();
            ExecuteReader($@"SELECT DISTINCT Version.service FROM Version INNER JOIN Software ON Version.idSoftware = {p_software};");
            while (_reader.Read())
            {
                v_readerVersion.Add(new string[] { _reader.GetString(0) });
            }
            _reader.Close();
            foreach (string[] row in v_readerVersion)
            {
                v_services.Add(row[0]);
            }
            return v_services;
        }
        public List<string> FetchAllServices()
        {
            List<string> v_services = new List<string>();
            List<Category> v_categories = FetchCategories();
            foreach(Category category in v_categories)
            {
                foreach(IsaSoftware isaSoftware in category.Softwares)
                {
                    foreach(IsaVersion isaVersion in isaSoftware.Versions)
                    {
                        v_services.Add(isaVersion.Service);
                    }
                }
            }

            return v_services;
        }

        public int AddCategory(string p_Name)
        {
            Guard.Against.NullOrEmpty(p_Name,  nameof(p_Name));

            try
            {
                return Execute($"Insert into Category (name) values('{p_Name}')");
            }catch(SQLiteException v_Ex)
            {
                throw v_Ex;
            }
        }
    }
}
