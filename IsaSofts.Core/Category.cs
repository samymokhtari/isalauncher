﻿using System.Collections.Generic;

namespace IsaSofts.Core
{
    public class Category
    {
        private int _id;
        private string _name;
        private List<IsaSoftware> _softwares;

        public int Id { get => _id; set => _id = value; }
        public string Name { get => _name; set => _name = value; }
        public List<IsaSoftware> Softwares { get => _softwares; set => _softwares = value; }

        public Category(int p_id, string p_name)
        {
            Id = p_id;
            Name = p_name;
        }
        public Category(int p_id,  string p_name, List<IsaSoftware> p_softwares)
        {
            Id = p_id;
            Name = p_name;
            Softwares = p_softwares;
        }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
