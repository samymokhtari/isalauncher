﻿using IsaSofts.Core;
using IsaSofts.Core.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;
using System.Windows;
using System.Windows.Controls;

namespace IsaSofts.MVVM.View
{
    /// <summary>
    /// Logique d'interaction pour HomeView.xaml
    /// </summary>
    public partial class HomeView : UserControl
    {
        public List<Category> Categories { get; set; }
        public int CategoryId { get; set; }  = -1;
        public Log Log { get; set; } = new Log();
        public int SoftwareId { get; set; } = -1;
        public int VersionId { get; set; } = -1;
        public DatabaseManager DatabaseManager { get; set; }
        public HomeView()
        {
            InitializeComponent();
            try
            {
                DatabaseManager = DatabaseManagerSingleton.GetInstance();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Erreur de connexion à base de données SQLite. Plus d'informations dans le fichier de log au chemin suivant %FOLDERAPPLICATION%/logs/logs-ddMMyy.log");
                Log.Write("ERROR", ex.Message);
                Application.Current.Shutdown();
            }
            Categories = DatabaseManager.FetchCategories();
            FillCategories();
        }

        private void cmbCategory_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            CategoryId = Convert.ToInt32(cmbCategory.SelectedIndex);
            FillSoftwares();
            cmbSoftware.SelectedIndex = 0;
        }

        private void FillSoftwares()
        {
            if (CategoryId != -1)
            {
                cmbSoftware.ItemsSource = Categories[CategoryId].Softwares;
            }
        }

        private void cmbSoftware_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            SoftwareId = Convert.ToInt32(cmbSoftware.SelectedIndex);
            FillVersions();
            cmbVersion.SelectedIndex = 0;
        }

        private void cmbVersion_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            VersionId = Convert.ToInt32(cmbVersion.SelectedIndex);
        }

        private void FillCategories()
        {
            cmbCategory.ItemsSource = Categories;
        }

        private void FillVersions()
        {
            if (CategoryId != -1 && SoftwareId != -1)
            {
                cmbVersion.ItemsSource = Categories[CategoryId].Softwares[SoftwareId].Versions;
            }
        }

        private void btnStopServices_Click(object sender, RoutedEventArgs e)
        {
            lstServices.Items.Clear();
            for (int index = 0; index < Categories.Count; index++)
            {
                StopServices(index);
            }
            MessageBox.Show("Tous les services ont bien été arrêtés", "Message", new MessageBoxButton());
        }

        private void StopServices(int p_category)
        {
            foreach (IsaSoftware software in Categories[p_category].Softwares)
            {
                foreach (IsaVersion version in software.Versions)
                {
                    StopService(version.Service, 10000);
                }
            }
        }

        private void StartServices(int p_category)
        {
            foreach (IsaSoftware software in Categories[p_category].Softwares)
            {
                foreach (IsaVersion version in software.Versions)
                {
                    StartService(version.Service, 10000);
                }
            }
        }

        public void StartSoftware(int p_version)
        {
            try
            {
                Process.Start(Categories[CategoryId].Softwares[SoftwareId].Versions[p_version].Path, Categories[CategoryId].Softwares[SoftwareId].Versions[p_version].Arguments);
                lstServices.Items.Add($"Logiciel {Categories[CategoryId].Softwares[SoftwareId].Name} : {Categories[CategoryId].Softwares[SoftwareId].Versions[p_version].Name} demarré.");
                Log.Write("INFO", $"The software {Categories[CategoryId].Softwares[SoftwareId].Versions[p_version].Name} has been launched.");
            }
            catch (Exception ex)
            {
                lstServices.Items.Add($"Logiciel {Categories[CategoryId].Softwares[SoftwareId].Versions[p_version].Name} est introuvable ou ne peut pas être demarré.");
                Log.Write("ERROR", $"The software {Categories[CategoryId].Softwares[SoftwareId].Versions[p_version].Name} cannot be launched: {ex.Message}");
                Console.WriteLine(ex);
            }
        }

        private void btnStartServices_Click(object sender, RoutedEventArgs e)
        {
            if (CategoryId != -1)
            {
                lstServices.Items.Clear();
                StopServices(CategoryId);
                StartServices(CategoryId);
                StartSoftware(VersionId);
            }
            else
            {
                MessageBox.Show("Veuillez choisir un logiciel avant de valider votre choix.", "Attention", new MessageBoxButton());
            }
        }

        private void btnServices_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Button srcButton = e.Source as Button;
            srcButton.Foreground = System.Windows.Media.Brushes.Black;
        }

        private void btnServices_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Button srcButton = e.Source as Button;
            srcButton.Foreground = System.Windows.Media.Brushes.White;
        }

        private void StartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                Log.Write("INFO", $"The service {serviceName} has been started");
                lstServices.Items.Add($"Service {serviceName} demarré.");
            }
            catch (System.ServiceProcess.TimeoutException ex)
            {
                lstServices.Items.Add($"Service {serviceName} ne peut pas être demarré. Délai d'attente dépassé.");
                Log.Write("ERROR", $"The service {serviceName} cannot be launched: {ex.StackTrace}");
            }
            catch (InvalidEnumArgumentException ex)
            {
                lstServices.Items.Add($"Service {serviceName} ne peut pas être demarré. Argument non valide.");
                Log.Write("ERROR", $"The service {serviceName} cannot be launched: {ex.StackTrace}");
            }
            catch (Exception ex)
            {
                lstServices.Items.Add($"Service {serviceName} ne peut pas être demarré.");
                Log.Write("ERROR", $"The service {serviceName} cannot be launched: {ex.StackTrace}");
            }
        }

        private void StopService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                Log.Write("INFO", $"The service {serviceName} has been stopped");
                lstServices.Items.Add($"Service {serviceName} stoppé.");
            }
            catch (System.ServiceProcess.TimeoutException ex)
            {
                lstServices.Items.Add($"Service {serviceName} ne peut pas être stoppé. Délai d'attente dépassé");
                Log.Write("ERROR", $"The service {serviceName} cannot be stopped: {ex.Message}");
            }
            catch (InvalidEnumArgumentException ex)
            {
                lstServices.Items.Add($"Service {serviceName} ne peut pas être stoppé. Argument non valide.");
                Log.Write("ERROR", $"The service {serviceName} cannot be stopped: {ex.StackTrace}");
            }
            catch (Exception ex)
            {
                lstServices.Items.Add($"Service {serviceName} ne peut pas être stoppé.");
                Log.Write("ERROR", $"The service {serviceName} cannot be launched: {ex.StackTrace}");
            }
        }
    }
}