﻿using Ardalis.GuardClauses;
using IsaSofts.Core;
using IsaSofts.Core.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace IsaSofts.MVVM.View
{
    /// <summary>
    /// Logique d'interaction pour ParameterView.xaml
    /// </summary>
    public partial class ParameterView : UserControl, INotifyPropertyChanged
    {
        public List<Category> Categories { get; set; }
        public Log Log { get; set; } = new Log();
        public int CategoryId { get; set; } = -1;
        public DatabaseManager DatabaseManager { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string _textCategory;

        public string TextCategory
        {
            get { return _textCategory; }
            set
            {
                if (value != _textCategory)
                {
                    _textCategory = value;
                    OnPropertyChanged("TextCategory");
                }
            }
        }
        public ParameterView()
        {
            InitializeComponent();
            try
            {
                DatabaseManager = DatabaseManagerSingleton.GetInstance();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Erreur de connexion à base de données SQLite. Plus d'informations dans le fichier de log au chemin suivant %FOLDERAPPLICATION%/logs/logs-ddMMyy.log");
                Log.Write("ERROR", ex.Message);
                Application.Current.Shutdown();
            }
            FillCategories();
        }

        private void cmbCategory_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if(cmbCategorySoftware.SelectedIndex != -1)
                CategoryId = Convert.ToInt32(cmbCategorySoftware.SelectedIndex);
            else
                CategoryId = Convert.ToInt32(cmbCategoryVersion.SelectedIndex);

        }
        private void FillCategories()
        {
            Categories = DatabaseManager.FetchCategories();
            cmbCategoryVersion.ItemsSource = cmbCategorySoftware.ItemsSource = Categories;
        }

        
        private void btnAddCategory_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtCategory.Text))
                if(DatabaseManager.AddCategory(txtCategory.Text) > 0)
                {
                    FillCategories();
                }
            else
            {
                MessageBox.Show($"La catégorie ne doit pas être vide", "Erreur de saisie");
            }
        }
    }
}
