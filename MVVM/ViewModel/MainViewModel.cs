﻿using IsaSofts.Core;

namespace IsaSofts.MVVM.ViewModel
{
    public class MainViewModel : ObservableObject
    {
        public HomeViewModel HomeVM { get; set; }
        public ParameterViewModel ParameterVM { get; set; }

        private object _currentView;

        public object CurrentView
        {
            get { return _currentView; }
            set 
            { 
                _currentView = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand HomeViewCommand { get; set; }
        public RelayCommand ParameterViewCommand { get; set; }

        public MainViewModel()
        {
            HomeVM = new HomeViewModel();
            ParameterVM = new ParameterViewModel();
            CurrentView = HomeVM;

            HomeViewCommand = new RelayCommand(o =>
            {
                CurrentView = HomeVM;
            });
            ParameterViewCommand = new RelayCommand(o =>
            {
                CurrentView = ParameterVM;
            });
        }
    }
}
