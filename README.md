# IsaSofts

**Outil destinée à lancer en quelques clics tout les logiciels correspondant à une configuration bien précise**

Langage _CSharp_ et _WPF_.

Développé par:

- Samy MOKHTARI (Étudiant à l'IUT d'Amiens)
- Guillaume LECLERC (Conseiller technique logiciel confirmé à ISAGRI).
