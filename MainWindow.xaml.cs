﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Diagnostics;
using IsaSofts.Core;
using IsaSofts.Core.Tools;
using System.Drawing;
using System.Windows.Controls;
using System.ServiceProcess;
using System.ComponentModel;
using System.Windows.Input;

namespace IsaSofts
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public static DatabaseManager DatabaseManager { get; set; } = DatabaseManagerSingleton.GetInstance();
        public Log Log { get; } = new Log();

        public MainWindow()
        {
            InitializeComponent();
            StyleProperty.OverrideMetadata(typeof(Window), new FrameworkPropertyMetadata
            {
                DefaultValue = FindResource(typeof(Window))
            });
            string result = Resource.script_data;
            Console.WriteLine(result);
            Bitmap bitmap = new Bitmap(Resource.logoIsagri);
            imgIsagri.Source = BitmapToBitmapImage.ToBitmapImage(bitmap);
            try{
                DatabaseManager.ConnectDB("base");
            } catch (Exception ex){
                MessageBox.Show($"Erreur de connexion à base de données SQLite. Plus d'informations dans le fichier de log au chemin suivant %FOLDERAPPLICATION%/logs/logs-ddMMyy.log");
                Log.Write("ERROR",ex.Message);
                Application.Current.Shutdown();
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            DatabaseManager.CloseDB();
            base.OnClosing(e);
        }

        private void CloseApp(object sender, MouseButtonEventArgs e)
        {
                Application.Current.Shutdown();
        }
        private void MinimizeApp(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
    }
}
