color 1A
:boucle
@echo off
cls
echo CHOIX APPLI ?
echo 1 : PREST
echo 2 : GED
echo 3 : INDIV
set /P x1x2x3=?
IF %x1x2x3% == 1 GOTO PREST
IF %x1x2x3% == 2 GOTO GED
IF %x1x2x3% == 3 GOTO INDIV

-----------------------------------------------------------------------------

:PREST
@echo off
cls
echo DEMARRAGE SERVICES VERSION ?
echo 1 : 1360400
echo 2 : 1383400
echo 3 : A
set /p x1x2x3=?
IF %x1x2x3% == 1 GOTO PREST1360
IF %x1x2x3% == 2 GOTO PREST1383
IF %x1x2x3% == 3 GOTO APREST


------------------------------------------------------------------

:PREST1360
@echo off

net stop IS-CP-13.83.300
net stop IS-GI-13.83.300
net start IS-CP-13.60.400
net start IS-GI-13.60.400
echo services 1360400
echo 1 : ISACOMPTA
echo 2 : ISAGI
echo 3 : A
set /p x1x2x3=?
IF %x1x2x3% == 1 GOTO COMPT1360
IF %x1x2x3% == 2 GOTO GI1360
IF %x1x2x3% == 3 GOTO A1360


:COMPT1360 
echo demarrage ISACOMPTA 1360400
echo lancement application
start /w C:\prest\1360400\isacowp.gi\Client\CO.Application.exe
GOTO PREST1360

:GI1360 
echo demarrage ISAGI 1360400
echo lancement application
start /w C:\prest\1360400\isagiwp\Client\GI.Application.exe
GOTO PREST1360

:A1360
echo arret service 1360400
@echo off
net stop IS-CP-13.60.400
net stop IS-GI-13.60.400
GOTO PREST

-------------------------------------------------------------------------------

:PREST1383
@echo off
net stop IS-CP-13.60.400
net stop IS-GI-13.60.400
net start IS-CP-13.83.400
net start IS-GI-13.83.400
echo services 1383300
echo 1 : ISACOMPTA
echo 2 : ISAGI
echo 3 : 
set /p x1x2x3=?
IF %x1x2x3% == 1 GOTO COMPT1383
IF %x1x2x3% == 2 GOTO GI1383
IF %x1x2x3% == 3 GOTO A1383


:COMPT1383 
echo demarrage ISACOMPTA 1383400
echo lancement application
start /w C:\prest\1383300\isagiwp\Client\CO.Application.exe
GOTO PREST1383

:GI1383 
echo demarrage ISAGI 1383400
echo lancement application
start /w C:\prest\1383300\isagiwp\Client\GI.Application.exe
GOTO PREST1383

:A1383
echo arret service 1383400
@echo off
net stop IS-CP-13.83.400
net stop IS-GI-13.83.400
GOTO PREST

-------------------------------------------------------------------------------

:APREST
echo retour menu principale
net stop IS-CP-13.60.400
net stop IS-GI-13.60.400
net stop IS-CP-13.83.400
net stop IS-GI-13.83.400

GOTO BOUCLE

Echo Execution terminee

------------------------------------------------------------------------------

:GED
@echo off
cls
echo DEMARRAGE SERVICES VERSION ?
echo 1 : 210400
echo 2 : 220100
echo 3 : 250000
echo 4 : A
set /p x1x2x3x4=?
IF %x1x2x3x4% == 1 GOTO GED210
IF %x1x2x3x4% == 2 GOTO GED220
IF %x1x2x3x4% == 3 GOTO GED250
IF %x1x2x3x4% == 4 GOTO AGED


-----------------------------------------------------------------

:GED210
@echo off
net start IS-GD-2.10.400
net stop IS-GD-2.20.100
net stop IS-GD-2.50.000
echo services 210400
echo 1 : ISAGED
echo 2 : A
set /p x1x2=?
IF %x1x2% == 1 GOTO G210
IF %x1x2% == 2 GOTO A210

:G210
echo demarrage ISAGED V2.10.400
echo lancement application
start /w D:\210400\ISAGDWP\Client\GD.Application.exe /IP.Authentication.DomainId=1 /IP.Authentication.DataBaseName=TEST1 /IP.Authentication.Login=gleclerc /IP.Authentication.Password=adminged /IP.Authentication.RadicalApplication=GD
GOTO 5

:A210
echo fermeture
@echo off
net stop IS-GD-2.10.400
net stop IS-GD-2.20.100
net stop IS-GD-2.50.000
GOTO GED

--------------------------------------------------------------------

:GED220
@echo off
net start IS-GD-2.20.100
net stop IS-GD-2.10.400
net stop IS-GD-2.50.000
echo services 220100
echo 1 : ISAGED
echo 2 : A
set /p x1x2=?
IF %x1x2% == 1 GOTO G220
IF %x1x2% == 2 GOTO A220

:G220
echo demarrage ISAGED V2.20.100
echo lancement application
start /w D:\IsaGDWP\Client\GD.Application.exe /IP.Authentication.DataSetLabel=TEST1 /IP.Authentication.DomainId=6289 /IP.Authentication.Login=adminged /IP.Authentication.Password=
GOTO GED220

:A220
echo fermeture
@echo off
net stop IS-GD-2.10.400
net stop IS-GD-2.20.100
net stop IS-GD-2.50.000
GOTO GED

--------------------------------------------------------------------

:GED250
@echo off
net start IS-GD-2.50.000
net stop IS-GD-2.10.400
net stop IS-GD-2.20.100
echo services 201100
echo 1 : ISAGED
echo 2 : A
set /p x1x2=?
IF %x1x2% == 1 GOTO G250
IF %x1x2% == 2 GOTO A250

:G250
echo demarrage ISAGED V2.50.000
echo lancement application
start /w D:\2_50\ISAGDWP\Client\GD.Application.exe /IP.Authentication.DataSetLabel=GED_2.50.000 /IP.Authentication.DomainId=6289 /IP.Authentication.Login=gleclerc /IP.Authentication.Password=
GOTO GED250

:A250
echo fermeture
@echo off
net stop IS-GD-2.10.400
net stop IS-GD-2.20.100
net stop IS-GD-2.50.000
GOTO GED

-----------------------------------------------------------------------

:AGED
echo retour menu principale
net stop IS-GD-2.10.400
net stop IS-GD-2.20.100
net stop IS-GD-2.50.000
GOTO boucle

Echo Execution terminee

-----------------------------------------------------------------------

:INDIV
@echo off
cls
echo DEMARRAGE SERVICES VERSION ?
echo 1 : CO1263001
echo 2 : CO1360400
echo 3 : GC1370101
echo 4 : A
set /p x1x2x3x4=?
IF %x1x2x3x4% == 1 GOTO CO1263
IF %x1x2x3x4% == 2 GOTO CO1360
IF %x1x2x3x4% == 3 GOTO GC1370
IF %x1x2x3x4% == 4 GOTO AINDIV


-----------------------------------------------------------------

:CO1263
@echo off
net start IS-Isacompta-12.63.001
net stop IS-CO-13.60.400
net stop IS-CO-GC-13.70.011
net stop IS-GC-13.70.101
echo services 1263001
echo 1 : ISACOMPTA
echo 2 : A
set /p x1x2=?
IF %x1x2% == 1 GOTO C1263
IF %x1x2% == 2 GOTO A1263

:C1263
echo demarrage ISACOMPTA 12.63.001
echo lancement application
start /w C:\Compta\V2015V6\isacowp\Client\CO.Application.exe
GOTO CO1263

:A1263
echo fermeture
@echo off
net stop IS-Isacompta-12.63.001
net stop IS-CO-13.60.400
net stop IS-CO-GC-13.70.011
net stop IS-GC-13.70.101
GOTO INDIV

--------------------------------------------------------------------

:CO1360
@echo off
net start IS-CO-13.60.400
net stop IS-Isacompta-12.63.001
net stop IS-CO-GC-13.70.011
net stop IS-GC-13.70.101
echo services 13.60.400 INDIV
echo 1 : ISACOMPTA
echo 2 : A
set /p x1x2=?
IF %x1x2% == 1 GOTO C1360
IF %x1x2% == 2 GOTO A1360

:C1360
echo demarrage ISACOMPTA 13.60.400
echo lancement application
start /w C:\Compta\V2018\isacowp\Client\CO.Application.exe
GOTO CO1360

:A1360
echo fermeture
@echo off
net stop IS-Isacompta-12.63.001
net stop IS-CO-13.60.400
net stop IS-CO-GC-13.70.011
net stop IS-GC-13.70.101
GOTO INDIV

--------------------------------------------------------------------

:GC1370
@echo off
net start IS-CO-GC-13.70.011
net start IS-GC-13.70.101
net stop IS-CO-13.60.400
net stop IS-Isacompta-12.63.001
echo services 13.70.101 GC
echo 1 : GC-ISACOMPTA
echo 2 : GC
echo 3 : A
set /p x1x2x3=?
IF %x1x2x3% == 1 GOTO CO1370
IF %x1x2x3% == 2 GOTO G1370
IF %x1x2x3% == 3 GOTO A1370

:CO1370
echo demarrage GC-ISACOMPTA 13.70.101
echo lancement application
start /w C:\FACT\V2018\isacopgc\Client\CO.Application.exe
GOTO GC1370

:G1370
echo demarrage GC 13.70.101
echo lancement application
start /w C:\FACT\V2018\IsaGCWp\Client\GC.Application.exe
GOTO GC1370

:A1370
echo fermeture
@echo off
net stop IS-Isacompta-12.63.001
net stop IS-CO-13.60.400
net stop IS-CO-GC-13.70.011
net stop IS-GC-13.70.101

GOTO boucle

Echo Execution terminee

--------------------------------------------------------------------



